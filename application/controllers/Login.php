<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		not_login();
        $this->load->model('M_login');
		
	}


    public function index()
    {
        is_login();
        $this->load->view('admin/login');
       
    }

    public function action(){
		$nip = $this->input->post('nip', TRUE);
        $password = $this->input->post('password', TRUE);

		//ini untuk dummy yg di cek cm NIP nya aja
		$cek = $this->M_login->cekLoginDummy($nip);

		// ini yang asli
		// $cek = $this->M_login->cek_login_sinaga($nip, $password);
		
		if($cek != null){

			$A_01 = getDataMastfip($cek->nip)->A_01;
			$B_09 = getDataMastfip($cek->nip)->B_09;
			$I_5A = getDataMastfip($cek->nip)->I_5A;
			$B_02B = getDataMastfip($cek->nip)->B_02B;

			$session = array(
				'id' => $cek->id,
				'A_01' => $A_01,
				'B_02B'	=> $B_02B,
				'role' => $cek->role
			);

			$this->session->set_userdata($session);
            $messge = array(
                'status' => 'success',
                'type' => 'success',
                'message' => 'Selamat Datang Kembali di Apalikasi Pensiun'
            );
			$this->session->set_flashdata($messge);
			redirect('dashboard');
		} else {
			//pass salah
			// echo 'salah';
			$messge = array(
                'status' => 'error',
                'type' => 'danger',
                'message' => 'periksa kembali username dan password anda'
            );

			$this->session->set_flashdata($messge);
			redirect('login');
		}
	}

    public function logout(){
        if(isset($_SESSION['B_02B'])) {

			$array_items = array(
				'id',
				'A_01',
				'B_02B',
				'role',
				'kembali_url'
			);
			$this->session->unset_userdata($array_items);

			redirect('/');
		} else {
			redirect('/');
		}
    }
    

    

    
}
