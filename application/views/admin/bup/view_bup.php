<div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Data Nominatif Calon Pensiun BUP - SKPD</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Mutasi PNS Masuk</li>
						<li class="breadcrumb-item active">List </li>
					</ol>
				</div>
			</div>
			<?php
				$info= $this->session->flashdata('info');
				$pesan= $this->session->flashdata('pesan');

				if($info == 'success'){ ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
					</div>
				<?php    
				}elseif($info == 'danger'){ ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
					</div>
			<?php  }else{ } ?>
		</div>
	<!-- /.container-fluid -->
	</section>
    
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Data Tabel Calon BUP-SKPD</h3><br><br>
							<!-- <a href="<?= site_url('')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a> -->
						</div>
							<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
						<!-- /.card-header -->	
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>NIP</th>
										<th>Nama</th>
										<th>Pangkat/Jabatan</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead> 
								<tbody>
							
									<tr>
										<td>a</td>
										<td>b</td>
										<td>c</td>
										<td>d</td>
										<td>e</td>
										<td>
											<div class="btn-group">
												<button type="button" class="btn btn-sm btn-success">Periksa Data</button>
												<button type="button" class="btn btn-success dropdown-toggle dropdown-hover dropdown-icon" data-toggle="dropdown">
												<span class="sr-only">Toggle Dropdown</span>
												</button>
												<div class="dropdown-menu" role="menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>NIP</th>
										<th>Nama</th>
										<th>Pangkat/Jabatan</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
						</div>
					<!-- /.card-body -->
					</div>
				<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
  <!-- /.content-wrapper -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "buttons": ["csv", "excel", "pdf"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('#example3').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
