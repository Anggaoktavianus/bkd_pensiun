
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data User </li>
            </ol>
          </div>
        </div>
        <?php
					$info= $this->session->flashdata('info');
					$pesan= $this->session->flashdata('pesan');

					if($info == 'success'){ ?>
						<div class="alert alert-success">
  						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  						  <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
  						</div>
					<?php    
					}elseif($info == 'danger'){ ?>
						<div class="alert alert-danger">
  						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
						</div>
					<?php  }else{ } ?>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Tabel User</h3><br><br>
                <a href="<?= site_url('user/add_index')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a>
              </div>
             
              <!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Email</th>
                    <th>Nama</th>
                    <th>Status</th>
                    <th>Level</th>
                    <th>Action</th>
                  </tr>
                  </thead> 
                  <tbody>
                    <?php if ($this->fungsi->user_login()->role_id == 1  ) { ?>
                    <?php $no = 1;
                      foreach ($row->result() as $key => $data) {
                    ?>
                  <tr>
                    <td><?= $no++ ?></td>
                        <td><?= $data->email ?></td>
                        <td><?= $data->nama ?></td>
                        <td><?= $data->status == 1 ? "Aktif" : "Nonaktif" ?></td>
                        <td><?= $data->role ?></td>
                        <td>
                        <form action="<?= site_url('user/del') ?>" method="post">
                           <a href="<?= site_url('user/reset/' . $data->id) ?>"><button onclick="return confirm('Anda ingin reset password menjadi 12345 ?')" type="button" class="btn btn-sm btn-primary"><i class="fas fa-key" style="font-size:12px"></i></button></a> 
                           <a href="<?= site_url('user/edit/' . $data->id) ?>" class="btn btn-success btn-sm text-white">
                        <i class="fas fa-pencil-alt "></i>
                        </a>
                        <input type="hidden" name="id" value="<?= $data->id ?>">
                        <button onclick="return confirm('Apakah anda yakin?')" class="btn btn-danger btn-sm text-white">
                        <i class="fas fa-trash-alt "></i>
                        </button>
                        </form>
                        </td>
                  </tr>
                   <?php
                    } ?>
                 <?php } ?>
                 <?php if ($this->fungsi->user_login()->role_id == 2  ) { ?>
                    <?php $no = 1;
                      foreach ($rows as $key => $data) {
                    ?>
                  <tr>
                    <td><?= $no++ ?></td>
                        <td><?= $data->email ?></td>
                        <td><?= $data->nama ?></td>
                        <td><?= $data->status == 1 ? "Aktif" : "Nonaktif" ?></td>
                        <td><?= $data->role ?></td>
                        <td>
                                
                        <form action="<?= site_url('user/del') ?>" method="post">
                           <a href="<?= site_url('user/reset/' . $data->id) ?>"><button onclick="return confirm('Anda ingin reset password menjadi 12345 ?')" type="button" class="btn btn-sm btn-primary"><i class="fas fa-key" style="font-size:12px"></i></button></a> 
                           <a href="<?= site_url('user/edit/' . $data->id) ?>" class="btn btn-success btn-sm text-white">
                        <i class="fas fa-pencil-alt "></i>
                        </a>
                        <input type="hidden" name="id" value="<?= $data->id ?>">
                        <button onclick="return confirm('Apakah anda yakin?')" class="btn btn-danger btn-sm text-white">
                        <i class="fas fa-trash-alt "></i>
                        </button>
                        </form>
                        </td>
                  </tr>
                   <?php
                    } ?>
                 <?php } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Email</th>
                    <th>Nama</th>
                    <th>Status</th>
                    <th>Level</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->