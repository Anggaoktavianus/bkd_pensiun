<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Kontak Kami</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Kontak</li>
            </ol>
          </div>
        </div>
        <?php
          $info= $this->session->flashdata('info');
          $pesan= $this->session->flashdata('pesan');

          if($info == 'success'){ ?>
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
            </div>
          <?php    
          }elseif($info == 'danger'){ ?>
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
			</div>
		      <?php  }else{ } ?>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <!-- Input addon -->
		<div class="card card-info">
			<div class="card-header">
				<h3 class="card-title">Hubungi Kami </h3>
			</div>
			<form action="" method="POST" enctype="multipart/form-data">
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="id" class="form-control"  value="<?= $row->id ?>" >
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-envelope"></i></span>
								</div>
									<input type="email" name="email" class="form-control" value="<?= $row->email;?>" placeholder="Email">
								<?= form_error('email', '<div class="text-danger"><small>', '</small></div>') ?>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-phone"></i></span>
								</div>
									<input type="text" name="telepon" class="form-control" value="<?= $row->telepon;?>" placeholder="Email">
								<?= form_error('telepon', '<div class="text-danger"><small>', '</small></div>') ?>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-globe"></i></span>
								</div>
									<input type="text" name="web" class="form-control" value="<?= $row->web;?>" placeholder="Website">
								<?= form_error('web', '<div class="text-danger"><small>', '</small></div>') ?>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-address-card"></i></span>
								</div>
								<textarea class="form-control" name="alamat" rows="5"  placeholder="Masukkan alamat ..."><?= $row->alamat ?></textarea>	
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fab fa-whatsapp"></i></span>
								</div>
									<input type="text" name="wa" class="form-control" value="<?= $row->wa;?>" placeholder="Whatsapp">
								<?= form_error('wa', '<div class="text-danger"><small>', '</small></div>') ?>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fab fa-instagram"></i></span>
								</div>
									<input type="text" name="ig" class="form-control" value="<?= $row->ig;?>" placeholder="Instagram">
								<?= form_error('ig', '<div class="text-danger"><small>', '</small></div>') ?>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fab fa-twitter"></i></span>
								</div>
									<input type="text" name="twitter" class="form-control" value="<?= $row->twitter;?>" placeholder="Twitter">
								<?= form_error('twitter', '<div class="text-danger"><small>', '</small></div>') ?>
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-map"></i></span>
								</div>
								<textarea class="form-control" name="lokasi" rows="5"  placeholder="Masukkan alamat ..."><?= $row->lokasi ?></textarea>	
							</div>
						</div>
					</div>
					<div class="input-group mb-3">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</div>
			</form>
		</div>
        </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

    </div>
  