<?php
    function getDataMastfip($nip){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE B_02B = ?";

        $query = $dbeps->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getDataMastfipByI_05($I_05){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE I_05 = ?";

        $query = $dbeps->query($sql, array($I_05));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getKepalaDinasByA01A05($A01A05){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE CONCAT(A_01,A_02,A_03,A_04,A_05) = ?
        AND I_5A = '1'";

        $query = $dbeps->query($sql, array($A01A05));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getKepalaDinasPenggantiByA01A05($A01A05){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbsinaga = $ci->load->database('sinaga', TRUE);
        

        $sql = 
        "SELECT *
        FROM `pejabat_pengganti`
        WHERE unitkerja = ?";

        $query = $dbsinaga->query($sql, array($A01A05));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function kodestaff(){

        $data = 'B200201000';
        return $data;
    }

    function getDataMastfipByAstaff(){
        // get data mastfip by kolom A 1 - 5 (Pencarian Staff Subbidang Formasi)
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT * FROM MASTFIP08 WHERE A_01 = 'B2' AND A_02 = 00 AND A_03 = 20 AND A_04 = 10 AND A_05 = 00 AND I_5A != 1";

        $query = $dbeps->query($sql);
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getDataMastfipByAstr($A_01,$A_02,$A_03,$A_04,$A_05,$I_5A){
        // get data mastfip by kolom A 1 - 5 (Pencarian Struktural)
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE A_01 = ?
        AND A_02 = ?
        AND A_03 = ?
        AND A_04 = ?
        AND A_05 = ?
        AND I_5A = ?
        ";

        $query = $dbeps->query($sql, array($A_01,$A_02,$A_03,$A_04,$A_05,$I_5A));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getDataUserNewEps($nip){
        $ci = get_instance();

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `USER_NEW`
        WHERE username = ?";

        $query = $dbeps->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function getUmpegMastfip($A_01){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);
        

        $sql = 
        "SELECT *
        FROM `MASTFIP08`
        WHERE A_01 = ?";

        $query = $dbeps->query($sql, array($A_01));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function formatTulisNIP($nip){
        $fixnip = '';

        $first = substr($nip, 0, 8);
        $second = substr($nip, 8, 6);
        $third = substr($nip, 14, 1);
        $fourth = substr($nip, 15, 3);

        $fixnip = $first.' '.$second.' '.$third.' '.$fourth;

        return $fixnip;
    }

    function getPLT($nip){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbsinaga = $ci->load->database('sinaga', TRUE);
        

        $sql = 
        "SELECT *
        FROM pejabat_pengganti
        WHERE nip_pengganti = ?";

        $query = $dbsinaga->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    function kodeBidangFormasiPengembangan(){
        $kode = 'B200201000';
        return $kode;
    }

    function namaBidangFormasiPengembangan(){
        $kode = 'STAFF SUB BIDANG FORMASI DAN PENGEMBANGAN';
        return $kode;
    }
?>